# encoding=UTF-8
'''
Demo usage
'''

import os
import string

from sanitize import sanitize

if __name__ == '__main__':
    print '-- Canned tests --'
    lines = [# Translates to "I'm three years old," but transliterates to "I
             # have three anuses."  Oh, well.
             u'¿Hola, cómo estás? Tengo tres años.',
             
             # Saw this one before (Portuguese)
             u'aplicação',
             
             # This was the example from a blog admin that had a similar need.
             # It's Swedish, not German, so it maps correctishly.  Since most
             # languages with this diacritic (umlaut and whatever else it's
             # called--I can't recall right now) do not transliterate to the
             # German-only transliteration/approximate of adding an e after the
             # undecorated letter, and since it's well beyond our ability and
             # scope to detect language during sanitization, we'll opt for the
             # transliteration that preserves the number of bytes. (So "ö" will
             # become just "o" and not "oe".)
             u'“Klüft skräms inför på fédéral électoral große”',
            ]
    
    # Load the test file, too
    with open('test.txt') as f:
        lines.append(f.read())
    
    for line in lines:
        print '"%s"%s -> "%s"' % (line, os.linesep, sanitize(line))
    
    print '-- Testing ranges --'
    for char_class in [string.printable]:
        print 'Unmodified: %s' % char_class
        print 'Sanitized:  %s' % sanitize(char_class)

